%define OFFSET 8

section .text

global find_word

extern string_equals


; rdi <-- key pointer = null-terminated string
; rsi <-- list head pointer
; rax <-- if (element was found) address; else 0
find_word:
	
.loop:
	test rsi, rsi		; if it`s last element -> go to .end_list
	je .end_list
	
	push rsi		; save rsi, rdi (caller-saved regs)
	push rdi	
	add rsi, OFFSET		; skip pointer to the next element
	call string_equals
	pop rdi
	pop rsi
	
	test rax, rax		; if we found equal string -> put address in rax
	jnz .string_found
	
	mov rsi, [rsi]		; [rsi] contains next element
	jmp .loop

.string_found:
	mov rax, rsi
	ret
	
.end_list:
	xor rax, rax
	ret

















	
