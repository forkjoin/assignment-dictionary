%include 'lib.inc'
%include 'word.inc'

%define STD_OUT 1
%define STD_ERR 2
%define NEWLINE_CHAR 10
%define BUF_SIZE 256

extern find_word

global _start
	
section .data
	
key:	db 'enter key: ', 0
not_found_msg: db 'not found', NEWLINE_CHAR, 0
err_msg: db 'error', NEWLINE_CHAR, 0
found_msg: db 'element was found:', NEWLINE_CHAR, 0

section .text

	
_start:
	mov rsi, STD_OUT
	mov rdi, key		; print greeting message
	call print_string
	
	sub rsp, BUF_SIZE	; allocate memory in stack
	mov rdi, rsp
	mov rsi, BUF_SIZE

	call read_word
	cmp rax, 0
	je .read_err
	
	mov rsi, head		; try to find word with that key in list
	mov rdi, rax
	call find_word

	test rax, rax
	jz .not_found

	push rax
	mov rdi, found_msg
	mov rsi, STD_OUT
	call print_string
	pop rax
	
	add rax, 8		; skip pointer

	mov rdi, rax
	call string_length
	
	add rdi, rax
	inc rdi
	
	call print_string
	
	jmp .end
.not_found:
	mov rsi, STD_ERR
	mov rdi, not_found_msg
	call print_string
	jmp .end
.read_err:
	mov rsi, STD_ERR
	mov rdi, err_msg
	call print_string
	jmp .end

.end:
	add rsp, 256
	call exit




































































	
