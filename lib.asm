%define STD_IN 0
%define STD_OUT 1

%define SYS_READ 0
%define SYS_WRITE 1
%define SYS_EXIT 60

section .data
section .text
	
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy


; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT           ; identifier of 'exit' syscall
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax		; clear rax

.loop:	     			
    cmp byte[rdi + rax], 0	; check if current symbol is null-terminator
    je .end
    inc rax			; continue with next symbol
    jmp .loop

.end:
    ret
    
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi			; caller-saved reg
    call string_length
    pop rdi
    mov rdx, rax		; amount of bytes to print		
    mov rsi, rdi		; string address
    mov rax, SYS_WRITE			
    mov rdi, STD_OUT
    syscall
    ret

; Принимает код символа в rdi и выводит его в stdout
print_char:
    mov rax, SYS_WRITE
    push rdi
    mov rsi, rsp		; char address
    mov rdi, STD_OUT
    mov rdx, 1			; amout of bytes to print (1)
    syscall
    pop rdi			; restore rdi register
    ret
    
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor r9, r9			; iterate counter
    mov r10, 10			; divider to get last digit
    push r9  			; null-terminator
    mov rax, rdi		

.loop:
    inc r9
    xor rdx, rdx
    div r10			; remainder in dl
    add dl, '0'			; convert digit to ASCII code
    dec rsp 			; allocate memory in stack
    mov [rsp], dl
    cmp rax, 0			
    jne .loop

.print:
    mov rdi, rsp		; string address
    push r9
    call print_string
    pop r9
    add rsp, r9			; restore rsp (calle-saved reg)
    pop r9
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
; Получаем указатель на число в rdi
print_int:
    cmp rdi, 0			; if number >= 0 -> print_uint(number)
    jge print_uint		; else print '-' and then print_uint(-number)
    
    push rdi			; save number
    mov rdi, '-'
    call print_char		; print '-'
    pop rdi
    neg rdi
    jmp print_uint		; print -number

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, SYS_READ
    mov rdi, STD_IN
    mov rdx, 1			; read 1 char
    mov rsi, rsp
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; получаем в rdi ссылку на буфер, в rsi его размер, возвращаем адрес буфера в rax, длину слова в rdx
read_word:
    xor r10, r10		; char counter

.skip_ws:
    push rsi			; save caller-saved regs
    push rdi
    push r10
    call read_char
    pop r10
    pop rdi
    pop rsi
    cmp r10, 0			; if r10 != 0 we need to check that r10 < buffer length
    jne .read_symbol
    cmp rax, ' '
    je .skip_ws
    cmp rax, `\t`
    je .skip_ws
    cmp rax, `\n`
    je .skip_ws

.read_symbol:
    cmp r10, rsi		; if r10 >= buffer length (rsi) -> go to .err
    jae .err
    cmp rax, ' '
    je .end
    cmp rax, `\t`
    je .end
    cmp rax, `\n`
    je .end
    mov byte [rdi + r10], al
    cmp rax, 0			; check current symbol is it null-terminator
    je .end
    inc r10
    jmp .skip_ws
    
.end:
    mov byte[rdi + r10], 0
    mov rdx, r10
    mov rax, rdi
    ret
    
.err: 
    xor rax, rax
    ret

; Принимает указатель на строку в rdi, пытается прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor r9, r9			; char buffer
    mov r10, 10			; multiplier
    xor rax, rax		; number
    xor rdx, rdx		; digit counter
    
.loop:
    mov r9b, byte[rdi + rdx]
    cmp r9b, '0'
    jl .end
    cmp r9b, '9'
    jg .end
    inc rdx
    sub r9b, '0'		; convert ASCII code to digit
    imul rax, r10
    add rax, r9
    jmp .loop
    
.end:
    ret

; Принимает указатель на строку, пытается прочитать из её начала знаковое число.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    cmp byte[rdi], '-'		; if first symbol != '-' -> go to parse_uint
    jne parse_uint 		; else ++rdi (skip '-') -> go to parse_uint -> -(rax) -> ++rdx (not to forget about '-')
    inc rdi
    push rdi
    call parse_uint
    pop rdi
    neg rax
    inc rdx
    ret

;  Принимает два указателя в rdi и rsi на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    call string_length		; check if str1.length == str2.length
    mov r10, rax
    push rdi
    mov rdi, rsi
    call string_length
    pop rdi
    cmp rax, r10
    jnz .failure
    xor r9, r9
    
.loop:
    mov cl, byte [rsi + r9]	; for i,j in str1, str2 if i != j -> go to .failure
    cmp cl, byte [rdi + r9]
    jne .failure
    cmp cl, 0
    je .succ
    inc r9
    jmp .loop
    
.succ:
    mov rax, 1
    ret

.failure:
    xor rax, rax
    ret
    
; Принимает указатель на строку в rdi, указатель на буфер в rsi и длину буфера в rdx
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length		; check if str.length != buffer length -> overflow
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    jge .OF
    push rbx
    xor rax, rax

.loop:				; for i in str -> copy to buffer
    mov bl, byte [rdi + rax]
    mov byte [rsi + rax], bl
    cmp byte [rdi + rax], 0	; if current symbol is null-terminator -> go to end
    je .end
    inc rax
    jmp .loop
    
.OF:
    xor rax, rax
    ret
    
.end:
    pop rbx
    ret
