lib.o: lib.asm
	nasm -f elf64 -o lib.o lib.asm
dict.o: dict.asm lib.o
	nasm -f elf64 -o dict.o dict.asm
main.o: main.asm dict.o word.inc
	nasm -f elf64 -o main.o main.asm
pr: main.o dict.o lib.o
	ld -o pr dict.o main.o lib.o
clean:
	rm -f ./*.o
