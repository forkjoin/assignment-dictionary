%define head 0
%macro colon 2											
	%ifid %2											
		%2: dq head									
		%define head %2						
	%else												
		%fatal "Incomatible type: second arg must be an id" 		
	%endif													
	%ifstr %1											
		db %1, 0										
	%else												
		%fatal "Incombatible type: first arg must be a string"	
	%endif												 	
%endmacro

